# Docker ModelSim

A Docker image with the ModelSim HDL simulator

## Run

Source `aliases.sh`:
```
source aliases.sh
```

Run simple HDL simulation:
```
vlib work
vlog src/test.v
vsim -c -do "run" test
```

Run ModelSim in graphical mode:
```
vsim
```

Run ModelSim Docker image:
```
./docker-run.sh
```
